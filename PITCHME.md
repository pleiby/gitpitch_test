# Flux Capacitors

An application architecture for Reactors

---

### Flux Design

- Dispatcher: sends dogcatcher
- Stores: Buy and sell stuff
- Views: seems good

---

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)
